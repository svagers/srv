"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var winston_1 = require("winston");
var Logger = /** @class */ (function () {
    function Logger(level) {
        if (level === void 0) { level = 'debug'; }
        var combine = winston_1.format.combine, timestamp = winston_1.format.timestamp, label = winston_1.format.label, printf = winston_1.format.printf;
        var log_format = printf(function (info) {
            return info.timestamp + " [" + info.label + "] " + info.level + ": " + info.message;
        });
        this.logger = winston_1.createLogger({
            level: level,
            format: combine(label({ label: 'chat server' }), timestamp(), log_format),
            transports: [
                new winston_1.transports.Console({
                    handleExceptions: true
                })
            ],
        });
    }
    Logger.prototype.log = function (message) {
        this.logger.log({
            level: 'info',
            message: message
        });
    };
    Logger.prototype.warn = function (message) {
        this.logger.log({
            level: 'warn',
            message: message
        });
    };
    Logger.prototype.error = function (message) {
        this.logger.log({
            level: 'error',
            message: message
        });
    };
    Logger.prototype.debug = function (message) {
        this.logger.log({
            level: 'debug',
            message: message
        });
    };
    return Logger;
}());
exports.default = new Logger();
