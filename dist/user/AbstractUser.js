"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TextMessage_1 = require("../message/TextMessage");
var moment = require("moment");
var AbstractUser = /** @class */ (function () {
    function AbstractUser(name) {
        this.name = name;
        this.signature = this.generateSignature();
        this.registerActivity();
    }
    AbstractUser.prototype.generateSignature = function () {
        return 'xxx';
    };
    AbstractUser.prototype.isValidSignature = function (signature) {
        return this.signature === signature;
    };
    AbstractUser.prototype.registerActivity = function () {
        this.lastActiveAt = moment();
    };
    AbstractUser.prototype.say = function (content, type) {
        this.registerActivity();
        var message;
        switch (type) {
            case "text":
                message = new TextMessage_1.TextMessage(this, content);
                break;
            default:
                message = new TextMessage_1.TextMessage(this, content);
                break;
        }
        return message;
    };
    return AbstractUser;
}());
exports.AbstractUser = AbstractUser;
