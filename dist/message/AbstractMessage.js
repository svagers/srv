"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractMessage = /** @class */ (function () {
    //protected time: moment;
    function AbstractMessage(author, content) {
        this.author = author;
        this.content = content;
        //this.time = moment.now();
    }
    AbstractMessage.prototype.toObject = function () {
        return {
            author: this.author.getName(),
        };
    };
    return AbstractMessage;
}());
exports.AbstractMessage = AbstractMessage;
