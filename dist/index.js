"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Chat_1 = require("./Chat");
require('dotenv').load();
var chat = new Chat_1.Chat();
var app = chat.getApp();
exports.app = app;
process.on('SIGTERM', function () {
    chat.disconnect();
});
process.on('SIGINT', function () {
    chat.disconnect();
});
