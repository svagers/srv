"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = /** @class */ (function () {
    function User(socket) {
        this.socket = socket;
        this.authorized = false;
        this.username = 'Anonymous';
        this.inactivity_limit = process.env.INACTIVITY_LIMIT || 1200000;
        this.registerActivity();
    }
    User.prototype.setUsername = function (username) {
        this.username = username;
        this.authorized = true;
    };
    User.prototype.registerActivity = function () {
        var _this = this;
        if (this.activity_timer !== null) {
            clearTimeout(this.activity_timer);
        }
        console.log('limit', this.inactivity_limit);
        this.activity_timer = setTimeout(function () {
            _this.send('activity_error', 'You have reached inactivity time limit. Disconnecting...');
        }, this.inactivity_limit);
    };
    User.prototype.warn = function (message) {
        this.socket.emit('warning', {
            message: message
        });
    };
    User.prototype.send = function (eventName, data) {
        this.socket.emit(eventName, data);
    };
    User.prototype.disconnect = function (reason) {
        this.socket.disconnect();
    };
    return User;
}());
exports.User = User;
