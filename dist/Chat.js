"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("http");
var express = require("express");
var socketIo = require("socket.io");
var logger_1 = require("./logger/logger");
var user_1 = require("./model/user");
var validate_js_1 = require("validate.js");
var authValidator_1 = require("./validators/authValidator");
var messageValidator_1 = require("./validators/messageValidator");
var Chat = /** @class */ (function () {
    function Chat() {
        this.users = [];
        this.app = express();
        this.server = http_1.createServer(this.app);
        this.port = process.env.PORT || 8084;
        this.io = socketIo(this.server);
        this.listen();
    }
    Chat.prototype.listen = function () {
        var _this = this;
        this.server.listen(this.port, function () {
            logger_1.default.log('Running server on port ' + _this.port);
        });
        this.io.on('connect', function (socket) {
            var user = new user_1.User(socket);
            logger_1.default.debug('new user connected');
            socket.on('message', function (message) {
                logger_1.default.debug(user.username + ' sent message');
                var errors = validate_js_1.validate(message, messageValidator_1.default);
                if (errors !== undefined) {
                    for (var i in errors.message) {
                        user.warn(errors.message[i]);
                    }
                    return;
                }
                if (user.authorized) {
                    user.registerActivity();
                    _this.sendMessage(message.message, user);
                }
                else {
                    user.warn('Unauthorized attempt to send messages');
                }
            });
            socket.on('auth', function (data) {
                logger_1.default.debug('someone is trying to authorize');
                var errors = validate_js_1.validate(data, authValidator_1.default);
                if (errors === undefined && _this.isUniqueName(data.username)) {
                    user.registerActivity();
                    user.setUsername(data.username);
                    _this.users.push(user);
                    user.send('auth', { username: data.username });
                    _this.sendInfo(user.username + ' Connected');
                }
                else {
                    user.send('auth_error', errors);
                    user.disconnect('Invalid username');
                }
            });
            socket.on('disconnect', function () {
                logger_1.default.debug('user disconnected');
                if (_this.users.indexOf(user) !== -1) {
                    _this.users.splice(_this.users.indexOf(user), 1);
                }
                if (user.authorized) {
                    _this.sendInfo(user.username + ' Disconnected');
                }
            });
        });
    };
    Chat.prototype.isUniqueName = function (username) {
        for (var i in this.users) {
            if (this.users[i].username === username) {
                return false;
            }
        }
        return true;
    };
    Chat.prototype.sendMessage = function (message, from) {
        var data = {
            from: from.username,
            message: message
        };
        this.io.emit('message', data);
    };
    Chat.prototype.sendInfo = function (message) {
        var data = {
            message: message
        };
        this.io.emit('info', data);
    };
    Chat.prototype.disconnect = function () {
        var _this = this;
        this.io.close(function () {
            logger_1.default.log('IO server closed.');
            _this.server.close(function () {
                logger_1.default.log('HTTP server closed.');
                process.exit(0);
            });
        });
    };
    Chat.prototype.getApp = function () {
        return this.app;
    };
    return Chat;
}());
exports.Chat = Chat;
