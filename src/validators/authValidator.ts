export default {
    username: {
        presence: true,
        format: {
            pattern: "[a-z0-9]+"
        },
        length: {
            minimum: 3,
            tooShort: 'Username must be between 3 and 12 characters long',
            tooLong: 'Username must be between 3 and 12 characters long',
        }
    }
}