export default {
    message: {
        presence: true,
        length: {
            minimum: 1,
            maximum: 200,
            tooShort: 'Message must be 1-200 characters long',
            tooLong: 'Message must be 1-200 characters long',
        }
    }
}