import { createLogger, format, transports } from 'winston';


class Logger {
  private logger: any;

  constructor(level: string = 'debug') {
    const { combine, timestamp, label, printf } = format;
    const log_format = printf((info: any) => {
      return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
    });

  	this.logger = createLogger({
      level: level,
      format: combine(
        label({ label: 'chat server' }),
        timestamp(),
        log_format
      ),
      transports: [
        new transports.Console({
          handleExceptions: true
        })
      ],
    });
  }

  log(message: string) {
    this.logger.log({
      level: 'info',
      message: message
    });
  }

  warn(message: string) {
    this.logger.log({
      level: 'warn',
      message: message
    });
  }

  error(message: string) {
    this.logger.log({
      level: 'error',
      message: message
    });
  }

  debug(message: string) {
    this.logger.log({
      level: 'debug',
      message: message
    });
  }
}

export default new Logger();