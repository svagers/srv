import { Chat } from './Chat';
require('dotenv').load();

let chat = new Chat();
let app = chat.getApp();
export { app };

process.on('SIGTERM', () => {
  chat.disconnect();
});

process.on('SIGINT', () => {
  chat.disconnect();
});