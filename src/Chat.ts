import { createServer, Server } from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io';
import logger from './logger/logger';
import { User } from './model/user';

import { validate } from 'validate.js';
import authValidator from './validators/authValidator'
import messageValidator from './validators/messageValidator'

export class Chat {
    private app: express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: string | number;

    private users: User[] = [];

    

    constructor() {
        this.app = express();
        this.server = createServer(this.app);
        this.port = process.env.PORT || 8084;
        this.io = socketIo(this.server);

        this.listen();
    }

    private listen(): void {
        this.server.listen(this.port, () => {
            logger.log('Running server on port ' + this.port);
        });

        this.io.on('connect', (socket: any) => {
            var user = new User(socket);
            logger.debug('new user connected');

            socket.on('message', (message: any) => {
                logger.debug(user.username + ' sent message');
                const errors = validate(message, messageValidator);

                if (errors !== undefined) {
                    for (var i in errors.message) {
                        user.warn(errors.message[i]);
                    }

                    return;
                }

                if (user.authorized) {
                    user.registerActivity();
                    this.sendMessage(message.message, user);
                } else {
                    user.warn('Unauthorized attempt to send messages')
                }
            });

            socket.on('auth', (data: any) => {
                logger.debug('someone is trying to authorize');
                const errors = validate(data, authValidator);

                if (errors === undefined && this.isUniqueName(data.username)) {
                    user.registerActivity();
                    user.setUsername(data.username);
                    this.users.push(user);

                    user.send('auth', { username: data.username });
                    this.sendInfo(user.username + ' Connected');
                } else {
                    user.send('auth_error', errors);
                    user.disconnect('Invalid username');
                }
            });

            socket.on('disconnect', () => {
                logger.debug('user disconnected');
                if (this.users.indexOf(user) !== -1) {
                    this.users.splice(this.users.indexOf(user), 1);
                }

                if (user.authorized) {
                    this.sendInfo(user.username + ' Disconnected');
                }
            });
        });
    }

    private isUniqueName(username: string): boolean {
        for (var i in this.users) {
            if (this.users[i].username === username) {
                return false;
            }
        }

        return true;
    }

    private sendMessage(message: string, from: User) {
        var data = {
            from: from.username,
            message: message
        }
        
        this.io.emit('message', data);
    }

    private sendInfo(message: string) {
        const data = {
            message: message
        };
        
        this.io.emit('info', data);
    }

    public disconnect() {
        this.io.close(() => {
            logger.log('IO server closed.');

            this.server.close(() => {
                logger.log('HTTP server closed.');
                process.exit(0);
            });
        });
    }

    public getApp(): express.Application {
        return this.app;
    }
}
