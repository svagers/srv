export class User {
	private activity_timer: any;
	public username: string;
	public authorized: boolean = false;
	private inactivity_limit: string | number;

    constructor(private socket: any) {
    	this.username = 'Anonymous';
    	this.inactivity_limit = process.env.INACTIVITY_LIMIT || 1200000;

    	this.registerActivity();
    }

    setUsername(username: string) {
    	this.username = username;
    	this.authorized = true;
    }

    registerActivity(): void {
		if (this.activity_timer !== null) {
			clearTimeout(this.activity_timer);
		}
		console.log('limit', this.inactivity_limit);

		this.activity_timer = setTimeout(() => {
			this.send('activity_error', 'You have reached inactivity time limit. Disconnecting...');
		}, this.inactivity_limit);
	}

	warn(message: string) {
		this.socket.emit('warning', { 
			message: message
		});
	}

	send(eventName: string, data: any) {
		this.socket.emit(eventName, data);
	}

	disconnect(reason: string) {
		this.socket.disconnect();
	}
}